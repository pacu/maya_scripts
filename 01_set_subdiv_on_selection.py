import pymel.core as pm

SUBDIV_TYPE = 1
SUBDIV_LEVEL = 2
UV_SMOOTH = None
# UV_SMOOTH = "pin_corners"

def setSubdiv(asset, subdiv_type, subdiv_level):
    """
    Sets the asset's subdiv type and level
    subdiv_type: 0 = no subdiv, 1 = catclark
    subdiv_level: subdiv level to apply, can get heavy FAST
    """
    asset.aiSubdivType.set(subdiv_type)
    asset.aiSubdivIterations.set(subdiv_level)
    if UV_SMOOTH:
        asset.aiSubdivUvSmoothing.set(UV_SMOOTH)


def inspect_asset(asset):
    """
    Either set the asset's subdiv or loops through its children to set theirs
    """
    if hasattr(asset, "aiSubdivType"):
        setSubdiv(asset, SUBDIV_TYPE, SUBDIV_LEVEL)
    children = asset.getChildren()
    if children:
        for child in children:
            inspect_asset(child)

for asset in pm.ls(sl=True):
    inspect_asset(asset)
