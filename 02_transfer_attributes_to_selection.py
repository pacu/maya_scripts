import maya.cmds as cmds


"""
transferAttributes doc :
https://download.autodesk.com/us/maya/2011help/CommandsPython/transferAttributes.html
"""


selected = cmds.ls(sl=True)
source = selected.pop(0)
for asset in selected:
    # transfer all uv from source to asset based on topology
    cmds.transferAttributes("{}".format(source), "{}".format(asset), transferUVs=2, sampleSpace=5)
