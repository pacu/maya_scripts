# maya_scripts

Collection of maya scripts for future use.

## scripts use

### 01 set subdiv on selection
change the SUBDIV_TYPE and SUBDIV_LEVEL arguments at the top of the script to chose which type and level to apply to your selection. The script will go down through groups to apply them to any item possible.

### 02 transfer attributes from source to selection list
Select your source first and then your entire selection. The script transfers all uv sets to the list using topology as a reference

### 03 Replace high or low suffixes on assets
Select all assets you want to rename. Then replace `_high` with `_low` or vice versa. Remove the brackets with the name to add suffix.
